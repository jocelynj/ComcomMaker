/*
[ tagSet with coma separator
overlay url
optional tags {"key":"value","key2":""}
]
*/
overlays=[
    // quartier
    ["type=boundary,boundary=administrative,admin_level=10"
    ,[
       "https://a.layers.openstreetmap.fr/admin10/${z}/${x}/${y}.png",
       "https://b.layers.openstreetmap.fr/admin10/${z}/${x}/${y}.png",
       "https://c.layers.openstreetmap.fr/admin10/${z}/${x}/${y}.png",
    ]],
    // commune
    ["type=boundary,boundary=administrative,admin_level=8"
    ,[
       "https://a.layers.openstreetmap.fr/admin8/${z}/${x}/${y}.png",
       "https://b.layers.openstreetmap.fr/admin8/${z}/${x}/${y}.png",
       "https://c.layers.openstreetmap.fr/admin8/${z}/${x}/${y}.png",
    ]],
    // arrondissement
    ["type=boundary,boundary=administrative,admin_level=7"
    ,[
       "https://a.layers.openstreetmap.fr/admin7/${z}/${x}/${y}.png",
       "https://b.layers.openstreetmap.fr/admin7/${z}/${x}/${y}.png",
       "https://c.layers.openstreetmap.fr/admin7/${z}/${x}/${y}.png",
    ]],
    // département
    ["type=boundary,boundary=administrative,admin_level=6"
    ,[
       "https://a.layers.openstreetmap.fr/admin6/${z}/${x}/${y}.png",
       "https://b.layers.openstreetmap.fr/admin6/${z}/${x}/${y}.png",
       "https://c.layers.openstreetmap.fr/admin6/${z}/${x}/${y}.png",
    ],
      {"name":"","wikipedia":"","website":"", "source":""}
    ],
    // ?
    ["type=boundary,boundary=administrative,admin_level=5"  
    ,[
       "https://a.layers.openstreetmap.fr/admin5/${z}/${x}/${y}.png",
       "https://b.layers.openstreetmap.fr/admin5/${z}/${x}/${y}.png",
       "https://c.layers.openstreetmap.fr/admin5/${z}/${x}/${y}.png",
    ]],
    // région
    ["type=boundary,boundary=administrative,admin_level=4"
    ,[
       "https://a.layers.openstreetmap.fr/admin4/${z}/${x}/${y}.png",
       "https://b.layers.openstreetmap.fr/admin4/${z}/${x}/${y}.png",
       "https://c.layers.openstreetmap.fr/admin4/${z}/${x}/${y}.png",
    ],
      {"name":"","wikipedia":"","website":"", "source":""}
    ],
    // communauté de commune
    ["type=boundary,boundary=local_authority,local_authority:FR=CC"
    ,[
                               "https://a.layers.openstreetmap.fr/boundary_local_authority/${z}/${x}/${y}.png",
                               "https://b.layers.openstreetmap.fr/boundary_local_authority/${z}/${x}/${y}.png",
                               "https://c.layers.openstreetmap.fr/boundary_local_authority/${z}/${x}/${y}.png",
    ],
      {"name":"","ref:INSEE":"","wikipedia":"","website":"", "source":""}
    ],
    // communauté d'agglomération'
    ["type=boundary,boundary=local_authority,local_authority:FR=CA"
    ,[
                               "https://a.layers.openstreetmap.fr/boundary_local_authority/${z}/${x}/${y}.png",
                               "https://b.layers.openstreetmap.fr/boundary_local_authority/${z}/${x}/${y}.png",
                               "https://c.layers.openstreetmap.fr/boundary_local_authority/${z}/${x}/${y}.png",
    ],
      {"name":"","ref:INSEE":"","wikipedia":"","website":"", "source":""}
    ],
    // communauté urbaine
    ["type=boundary,boundary=local_authority,local_authority:FR=CU"
    ,[
                               "https://a.layers.openstreetmap.fr/boundary_local_authority/${z}/${x}/${y}.png",
                               "https://b.layers.openstreetmap.fr/boundary_local_authority/${z}/${x}/${y}.png",
                               "https://c.layers.openstreetmap.fr/boundary_local_authority/${z}/${x}/${y}.png",
    ],
      {"name":"","ref:INSEE":"","wikipedia":"","website":"", "source":""}
    ],
    // canton
    ["type=boundary,boundary=political,political_division=canton"
    ,[
                               "https://a.layers.openstreetmap.fr/boundary_local_authority/${z}/${x}/${y}.png",
                               "https://b.layers.openstreetmap.fr/boundary_local_authority/${z}/${x}/${y}.png",
                               "https://c.layers.openstreetmap.fr/boundary_local_authority/${z}/${x}/${y}.png",
    ],
      {"name":"","wikipedia":"","website":"", "source":""}
    ],
    // Circonscriptions législatives
    ["type=boundary,boundary=political,political_division=circonscription_législative"
    ,[
                               "https://a.layers.openstreetmap.fr/boundary_local_authority/${z}/${x}/${y}.png",
                               "https://b.layers.openstreetmap.fr/boundary_local_authority/${z}/${x}/${y}.png",
                               "https://c.layers.openstreetmap.fr/boundary_local_authority/${z}/${x}/${y}.png",
    ]],
    // Paroisses catho
    ["type=boundary,boundary=religious_administration,religion=christian,denomination=catholic,admin_level=8"
    ,undefined,
      {"name":"","ref:CEF":"","contact:phone":"","contact:email":"","website":""},
    ],
    // Zone pastorale catho
    ["type=boundary,boundary=religious_administration,religion=christian,denomination=catholic,admin_level=7"
    ,undefined,
      {"name":"","ref:CEF":"","contact:phone":"","contact:email":"","website":""},
    ],
    // Diocèse catho
    ["type=boundary,boundary=religious_administration,religion=christian,denomination=catholic,admin_level=6"
    ,undefined,
      {"name":"","ref:CEF":"","contact:phone":"","contact:email":"","website":"","wikipedia":""},
    ],
]
