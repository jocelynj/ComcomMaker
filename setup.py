import setuptools

with open("README.txt", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="comcommaker",
    version="0.0.1",
    author="Vincent Pottier",
    author_email="author@example.com",
    description="ComcomMaker is an online tool for generating entities by merging relations",
    install_requires=[
        "osmapi",
    ],
    long_description=long_description,
    long_description_content_type="text/plain",
    url="https://gitlab.com/gileri/ComcomMaker",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.5',
)
